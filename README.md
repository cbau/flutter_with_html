# Flutter with HTML

Flutter app with and HTML header included when the target is web.

## Screenshots

<!--img src="./screenshots/home-small-light.png" alt="Home on small screens with light theme screenshot" width="240" /> <img src="./screenshots/home-small-dark.png" alt="Home on small screens with dark theme screenshot" width="240" /-->
<img src="./screenshots/home-big-light.png" alt="Home on big screens with light theme screenshot" width="480" /> <img src="./screenshots/home-big-dark.png" alt="Home on big screens with dark theme screenshot" width="480" />

## Features

- The app uses `HtmlElementView` to create a space for the header display on web target.
- The script is preloaded in `index.html` and called on Flutter after rendering.
- On targets that are not web, a native navigation is simulated instead.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/flutter_with_html/)

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
