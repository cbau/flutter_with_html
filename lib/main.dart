import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'presentation/main_app.dart';

void main() {
  GoRouter.optionURLReflectsImperativeAPIs = true;

  /*
  // Use `runWidget` instead of `runApp` to support the multi-view builder.
  runWidget(
    MultiViewBuilder(
      builder: (context) => const MainApp(),
    ),
  );
  */

  runApp(const MainApp());
}
