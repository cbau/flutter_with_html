import 'dart:js_interop';

@JS('getData')
external JSAny? _getData();

@JS('getPromisedData')
external JSPromise<JSAny>? _getPromisedData();

@JS('setData')
external void _setData(JSAny? data);

@JS('setPromisedData')
external JSPromise<JSAny>? _setPromisedData(JSAny? data);

Map<Object?, Object?>? getData() {
  final jsMap = _getData();
  final data = jsMap.dartify();
  if (data is Map) {
    return data;
  }
  return null;
}

Future<Map<Object?, Object?>?> getFutureData() async {
  final jsPromise = _getPromisedData();
  final jsMap = await jsPromise?.toDart;
  final data = jsMap.dartify();
  if (data is Map) {
    return data;
  }
  return null;
}

void setData(Map<String, Object?> data) {
  final jsMap = data.jsify();
  _setData(jsMap);
}

Future<void> setFutureData(Map<String, Object?> data) async {
  final jsMap = data.jsify();
  final jsPromise = _setPromisedData(jsMap);
  await jsPromise?.toDart;
}
