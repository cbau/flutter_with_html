Map<Object?, Object?>? getData() => null;

Future<Map<Object?, Object?>?> getFutureData() async => null;

void setData(Map<String, Object?> data) {}

Future<void> setFutureData(Map<String, Object?> data) async {}
