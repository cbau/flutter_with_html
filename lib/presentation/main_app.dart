import 'package:flutter/material.dart';

import 'app_router.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) => MaterialApp.router(
        darkTheme: ThemeData.dark(),
        routerConfig: AppRouter().router,
        title: 'Flutter with HTML',
        theme: ThemeData.light(),
      );
}
