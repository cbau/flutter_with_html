import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class NavigationPage extends StatelessWidget {
  const NavigationPage({
    required this.child,
    super.key,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) => Scaffold(
        body: child,
        bottomNavigationBar: kIsWeb
            ? null
            : NavigationBar(
                destinations: const [
                  NavigationDestination(
                    icon: Icon(Icons.home_outlined),
                    label: 'Home',
                    selectedIcon: Icon(Icons.home),
                  ),
                  NavigationDestination(
                    icon: Icon(Icons.info_outline),
                    label: 'Info',
                    selectedIcon: Icon(Icons.info),
                  ),
                  NavigationDestination(
                    icon: Icon(Icons.person_outline),
                    label: 'Profile',
                    selectedIcon: Icon(Icons.person),
                  ),
                ],
              ),
      );
}
