import 'package:go_router/go_router.dart';

import 'common/not_found_page.dart';
import 'item_details/item_details_page.dart';
import 'item_list/item_list_page.dart';
import 'navigation/navigation_page.dart';

class AppRouter {
  factory AppRouter() => _instance;

  AppRouter._();

  static final AppRouter _instance = AppRouter._();

  late final router = GoRouter(
    errorBuilder: (context, state) => const NotFoundPage(),
    routes: [
      GoRoute(
        builder: (context, state) => NavigationPage(
          child: ItemListPage(),
        ),
        name: ItemListPage.name,
        path: '/',
        routes: [
          GoRoute(
            builder: (context, state) => ItemDetailsPage(
              id: state.pathParameters['id'] ?? '0',
            ),
            name: ItemDetailsPage.name,
            path: ':id',
          ),
        ],
      ),
    ],
  );
}
