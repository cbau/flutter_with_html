import 'dart:js_interop';
import 'dart:ui_web';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:web/web.dart';

@JS('main')
external JSAny? _main();

class HtmlHeader extends StatefulWidget {
  const HtmlHeader({super.key});

  @override
  State<HtmlHeader> createState() => HtmlHeaderState();
}

class HtmlHeaderState extends State<HtmlHeader> {
  //final headerDiv = HTMLDivElement()..id = 'header';

  static bool isLoaded = false;

  @override
  Widget build(BuildContext context) {
    platformViewRegistry.registerViewFactory(
      'header',
      //(viewId) => headerDiv,
      (viewId) => HTMLDivElement()
        ..append(
          HTMLDivElement()..id = 'header',
        )
        ..append(
          HTMLScriptElement()
            ..addEventListener(
              'load',
              (Event event) {
                _main();
              }.toJS,
            )
            ..defer = true
            ..src = 'https://cbau.gitlab.io/flutter_with_html/header/script.js',
        ),
    );

    return const SizedBox(
      height: 120,
      child: HtmlElementView(viewType: 'header'),
    );
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      if (!isLoaded) {
        isLoaded = true;
        _main();
      }
    });
  }

  void removeElementId() {
    //headerDiv.id = '';
  }

  void restoreElementId() {
    //headerDiv.id = 'header';
  }
}
