import 'package:flutter/material.dart';

class HtmlHeader extends StatefulWidget {
  const HtmlHeader({super.key});

  @override
  State<HtmlHeader> createState() => HtmlHeaderState();
}

class HtmlHeaderState extends State<HtmlHeader> {
  @override
  Widget build(BuildContext context) => const SizedBox.shrink();

  void removeElementId() {}

  void restoreElementId() {}
}
