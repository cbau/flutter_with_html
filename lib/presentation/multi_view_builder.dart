import 'dart:ui';

import 'package:flutter/widgets.dart';

class MultiViewBuilder extends StatefulWidget {
  const MultiViewBuilder({
    required this.builder,
    super.key,
  });

  final WidgetBuilder builder;

  @override
  State<MultiViewBuilder> createState() => _MultiViewBuilderState();
}

class _MultiViewBuilderState extends State<MultiViewBuilder>
    with WidgetsBindingObserver {
  Map<Object, Widget> _views = <Object, Widget>{};

  @override
  Widget build(BuildContext context) => ViewCollection(
        views: _views.values.toList(
          growable: false,
        ),
      );

  @override
  void didChangeMetrics() {
    _updateViews();
  }

  @override
  void didUpdateWidget(MultiViewBuilder oldWidget) {
    super.didUpdateWidget(oldWidget);
    _views.clear();
    _updateViews();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _updateViews();
  }

  void _updateViews() {
    final newViews = <Object, Widget>{};
    for (final view in WidgetsBinding.instance.platformDispatcher.views) {
      final viewWidget = _views[view.viewId] ??
          _ViewBuilder(
            builder: widget.builder,
            flutterView: view,
          );
      newViews[view.viewId] = viewWidget;
    }
    setState(() {
      _views = newViews;
    });
  }
}

class _ViewBuilder extends StatelessWidget {
  const _ViewBuilder({
    required this.builder,
    required this.flutterView,
  });

  final FlutterView flutterView;
  final WidgetBuilder builder;

  @override
  Widget build(BuildContext context) => View(
        view: flutterView,
        child: Builder(
          builder: builder,
        ),
      );
}
