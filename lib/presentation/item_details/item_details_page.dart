import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../data/js_interop/js_interop.dart';
import '../common/html_header.dart';

class ItemDetailsPage extends StatelessWidget {
  const ItemDetailsPage({
    required this.id,
    super.key,
  });

  static const name = 'ItemDetailsPage';

  final String id;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: kIsWeb
            ? null
            : AppBar(
                title: const Text('Item details'),
              ),
        body: ListView(
          children: [
            const HtmlHeader(),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Item #$id',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id tincidunt mauris. Fusce semper vitae mi vel pellentesque. Quisque tempus diam at quam auctor pulvinar. Donec rhoncus nulla rhoncus eleifend consequat. Etiam ante risus, finibus id tristique et, tincidunt sit amet ligula. Etiam pretium volutpat vestibulum. Pellentesque ultrices consectetur interdum. Donec vitae libero odio. Sed vulputate suscipit rutrum. Suspendisse faucibus velit vel diam ultricies, a convallis neque volutpat. Pellentesque arcu odio, malesuada ut mauris ut, vestibulum hendrerit lacus.''',
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: [
                      ElevatedButton(
                        onPressed: () async {
                          debugPrint('Getting data…');
                          final data = await getFutureData();
                          debugPrint('dartData: $data');
                        },
                        child: const Text('Get data from JS'),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          debugPrint('Sending data…');
                          await setFutureData({
                            'id': 2,
                            'name': 'Mary',
                          });
                          debugPrint('Data sent');
                        },
                        child: const Text('Send data to JS'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}
