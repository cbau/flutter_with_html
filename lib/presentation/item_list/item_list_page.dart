import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../common/html_header.dart';
import '../item_details/item_details_page.dart';

class ItemListPage extends StatelessWidget {
  ItemListPage({super.key});

  static const name = 'ItemListPage';

  final htmlHeaderKey = GlobalKey<HtmlHeaderState>();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: kIsWeb
            ? null
            : AppBar(
                title: const Text('Items'),
              ),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: HtmlHeader(
                key: htmlHeaderKey,
              ),
            ),
            SliverList.builder(
              itemBuilder: (context, index) => ListTile(
                onTap: () async {
                  htmlHeaderKey.currentState?.removeElementId();
                  await GoRouter.of(context).pushNamed(
                    ItemDetailsPage.name,
                    pathParameters: {
                      'id': (index + 1).toString(),
                    },
                  );
                  htmlHeaderKey.currentState?.restoreElementId();
                },
                title: Text('Item #${index + 1}'),
              ),
              itemCount: 50,
            ),
          ],
        ),
      );
}
