function delay(timeout) {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

function getData() {
    return {
        'id': 1,
        'name': 'John'
    };
}

function setData(data) {
    console.log('jsData:', data);
}

async function getPromisedData() {
    await delay(500);
    return {
        'id': 1,
        'name': 'John'
    };
}

async function setPromisedData(data) {
    await delay(500);
    console.log(`jsData: ${JSON.stringify(data)}`);
}