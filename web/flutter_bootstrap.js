{{flutter_js}}
{{flutter_build_config}}

_flutter.loader.load({
  onEntrypointLoaded: async function onEntrypointLoaded(engineInitializer) {
    let engine = await engineInitializer.initializeEngine({
      hostElement: document.querySelector('#flutterTarget'),
      //multiViewEnabled: true,
    });
    let app = await engine.runApp();
    /*
    app.addView({
      hostElement: document.querySelector('#flutterTarget'),
    });
    */
  }
});
