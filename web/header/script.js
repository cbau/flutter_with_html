function main() {
    const header = document.getElementById('header');

    if (header === null) {
        console.error('No element found with the identifier "header". Please add one to load the content for the header.');
        return;
    }

    const title = document.createElement('h1');
    title.textContent = 'LOGO';
    header.appendChild(title);

    const nav = document.createElement('nav');
    header.appendChild(nav);

    const ul = document.createElement('ul');
    nav.appendChild(ul);

    for (let i = 0; i < 5; i += 1) {
        const li = document.createElement('li');
        ul.appendChild(li);

        const anchor = document.createElement('a');
        anchor.href = '#';
        anchor.textContent = `Menu ${i + 1}`;
        li.appendChild(anchor);

        const ul1 = document.createElement('ul');
        li.appendChild(ul1);

        for (let j = 0; j < i + 1; j += 1) {
            const li1 = document.createElement('li');
            ul1.appendChild(li1);

            const anchor1 = document.createElement('a');
            anchor1.href = '#';
            anchor1.textContent = `Submenu ${j + 1}`;
            li1.appendChild(anchor1);
        }
    }
}

window.addEventListener('load', main);
